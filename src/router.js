const Router = require('koa-router'),
  router = new Router(),
  model = require('./rhinoceros');

  const species = ['white_rhinoceros', 'black_rhinoceros', 'indian_rhinoceros', 'javan_rhinoceros', 'sumatran_rhinoceros']

router.get('/rhinoceros', (ctx, next) => {
  const rhinoceroses = model.getAll();
  ctx.response.body = { rhinoceroses };
});

router.post('/rhinoceros', (ctx, next) => {
  if(!ctx.request.body.name && !ctx.request.body.speices) ctx.throw(406, "Name and species are required")
  if(ctx.request.body.name.length > 20) ctx.throw(406, "Name cant'be longer than 20 characters")
  if(species.includes(ctx.request.body.species) === false) ctx.throw(406, "Rhinoceroses speices are white_rhinocero, black_rhinocero, idian_rhinocero, javan_rhinocero, or sumatran_rhinocero")
  ctx.response.body = model.newRhinoceros(ctx.request.body);
});

router.get(`/rhinocero/:id`, (ctx, next) => {
  const rhino = model.getOne(ctx.params.id);
  ctx.response.body = { rhino };
});

router.get('/rhinoceros/:filter', (ctx, nxt) =>{
  const rhinos = model.getFilterRhinos(ctx.params.filter);
  ctx.response.body = { rhinos };
})


router.get('/endangered', (ctx, nxt) =>{
  const rhinos = model.getEndangered();
  ctx.response.body = { rhinos }
})



module.exports = router;
