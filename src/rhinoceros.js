const uuidv4 = require('uuid/v4');
let rhinoceroses = require('./data');

exports.getAll = () => {
  return rhinoceroses;
};

exports.newRhinoceros = data => {
  const newRhino = {
    id: uuidv4(),
    name: data.name,
    species: data.species
  };
  rhinoceroses.push(newRhino);
  return newRhino;
};

exports.getOne = id =>{
  const rhino = rhinoceroses.find(rh => rh.id === id);
  return rhino;
}

exports.getFilterRhinos = data =>{
  const rhinos = rhinoceroses.filter(rh => rh.species === data);
  if(!rhinos){
    return rhinoceroses.filter(rh => rh.name === data);
  }
  return rhinos;
}

exports.getEndangered = () =>{
  let rhinoCounts = {};
  let endangerSpecies = [];

  for(let i = 0; i < rhinoceroses.length; i++){
    if(rhinoCounts[rhinoceroses[i].species]){
      rhinoCounts[rhinoceroses[i].species]++;
    }else{
      rhinoCounts[rhinoceroses[i].species] = 1;
    }
  }

  for(let rhino in rhinoCounts){
    if(rhinoCounts[rhino] < 3){
      endangerSpecies.push(rhino);
    }
  }
  const rhinos = rhinoceroses.filter(rhino => endangerSpecies.includes(rhino.species));
  return rhinos;
}
